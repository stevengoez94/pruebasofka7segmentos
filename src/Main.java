import java.util.*;
public class Main {

    public static void main(String[] args) {
        while (true){
            String[] fila1 = {"  -" ,"   ","  -" ,"  -" ,"   ","  -" ,"  -" ,"  -" ,"  -" ,"  -"};
            String[] fila3 = {"   " ,"   ","  -" ,"  -" ,"  -" ,"  -" ,"  -" ,"   ","  -" ,"  -"};
            String[] fila5 = {"  -"  ,"   ","  -" ,"  -" ,"   ","  -" ,"  -" ,"   ","  -" ,"  -"};

            System.out.println("Ingrese por favor 2 números separados por una coma \",\", el primer numero debe ser entre el 1 y el 10.");
            Scanner entrada = new Scanner(System.in);
            String digitos;

            digitos=entrada.next();
            if(!isNumeric(digitos)){
                System.out.println("Por favor ingrese solamente números, el primer digito debe ser del 1-10 seguido de una coma \",\" y luego ingresar otro número");
            } else{
                String[] datosEntrada = digitos.split(",");
                if(datosEntrada[0].equals("0") && datosEntrada[1].equals("0")){
                    break;
                }
                int size = Integer.parseInt(datosEntrada[0]);
                //limita los numeros al rango deseado
                if(size<1 || size>10){
                    System.out.println("Ingrese un número entero dentro del rango 1-10");

                } else {
                    String num=datosEntrada[1];
                    int tamanioNumero = num.length();
                    String lineaVertical =" | ";
                    for(int i=0;i<tamanioNumero;i++){
                        int digito=Integer.parseInt(String.valueOf(num.charAt(i)));
                        for(int j=0;j<size;j++){
                            System.out.print(fila1[digito]);
                        }
                        System.out.print("      ");
                    }
                    System.out.println();
                    for(int repetciones=0;repetciones<size;repetciones++) {
                        for (int i = 0; i < tamanioNumero; i++) {
                            int digito = Integer.parseInt(String.valueOf(num.charAt(i)));
                            if (digito == 1 || digito == 2 || digito == 3 || digito == 7) {
                                for (int j = 0; j < size; j++) {
                                    System.out.print("   ");
                                }
                                System.out.print(lineaVertical);
                            } else if (digito == 0 || digito == 4 || digito == 8 || digito == 9) {
                                System.out.print(lineaVertical);
                                for (int j = 1; j < size; j++) {
                                    System.out.print("   ");
                                }
                                System.out.print(lineaVertical);
                            } else {
                                System.out.print(lineaVertical);
                                for (int j = 0; j < size; j++) {

                                    System.out.print("   ");

                                }
                            }
                            System.out.print("   ");
                        }
                        System.out.println();
                    }


                    for(int i=0;i<tamanioNumero;i++){
                        int digito=Integer.parseInt(String.valueOf(num.charAt(i)));
                        for(int j=0;j<size;j++){
                            System.out.print(fila3[digito]);
                        }
                        System.out.print("      ");
                    }
                    System.out.println();

                    for(int repeticiones=0; repeticiones<size;repeticiones++) {
                        for (int i = 0; i < tamanioNumero; i++) {
                            int digito = Integer.parseInt(String.valueOf(num.charAt(i)));
                            if (digito == 1 || digito == 3 || digito == 4 || digito == 5 || digito == 7 || digito == 9) {

                                for (int j = 0; j < size; j++) {
                                    System.out.print("   ");

                                }
                                System.out.print(lineaVertical);
                            } else if (digito == 0 || digito == 6 || digito == 8) {
                                System.out.print(lineaVertical);
                                for (int j = 1; j < size; j++) {
                                    System.out.print("   ");
                                }
                                System.out.print(lineaVertical);

                            } else {
                                System.out.print(lineaVertical);
                                for (int j = 0; j < size; j++) {
                                    System.out.print("   ");
                                }
                            }
                            System.out.print("   ");
                        }
                        System.out.println();
                    }


                    for(int i=0;i<tamanioNumero;i++){
                        int digito=Integer.parseInt(String.valueOf(num.charAt(i)));
                        for(int j=0;j<size;j++){
                            System.out.print(fila5[digito]);
                        }
                        System.out.print("      ");
                    }
                    System.out.println();

                }


            }


        }

    }


    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            String[] numeros = cadena.split(",");
            Integer.parseInt(numeros[0]);
            Integer.parseInt(numeros[1]);
            resultado = true;
        } catch (Exception excepcion) {
            resultado = false;
        }

        return resultado;
    }
}